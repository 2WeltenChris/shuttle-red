Attention: ⚠️ means that a change breaks things. Manual adjustments will be necessary. So be careful before updating. Even data loss might occur.

Version 0.2.0 (19th of December 2022) ⚠️⚠️⚠️ 
  - refactoring (new node properties, therefore manual changing / checking shuttle-control nodes)
  - added node status
  - bugfix restart shuttle
