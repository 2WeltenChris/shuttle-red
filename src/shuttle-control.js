const npm = require('./util/npm')
const fs = require('fs')
const path = require('path')
const { fork } = require('child_process')

module.exports = function (RED) {
  function ShuttleControlNode (config) {
    RED.nodes.createNode(this, config)
    const node = this
    const getFlowShuttles = () => {
      if (!this.context().flow.get('shuttles')) {
        this.context().flow.set('shuttles', {})
      }
      return this.context().flow.get('shuttles')
    }
    let shuttles = getFlowShuttles()

    const listener = []
    // If shuttles are already running, we need to reattach the listeners
    Object.keys(shuttles).forEach((shuttleId) => {
      shuttles[shuttleId].on('message', (message) => {
        listener.forEach((receive) => {
          receive(message, shuttleId)
        })
      })
    })

    // Some helper methods for send & receive
    node.getRunningShuttles = () => Object.keys(shuttles)
    node.sendTo = (shuttleId, msg) => {
      shuttles[shuttleId]?.send(msg)
    }

    node.onMessage = (callback) => {
      listener.push(callback)
    }

    const startedStatus = { fill: 'green', shape: 'ring', text: 'Started' }
    const errorStatus = { fill: 'red', shape: 'ring', text: 'Error' }
    const setStatusToAllStartShuttleId = (shuttleId, status) => {
      // no RED.nodes.filterNodes on server side.
      RED.nodes.eachNode(n => {
        if (n.type === 'shuttle-control' && (n.action === 'start' || n.actionType === 'start')) {
          // only find if node project / shuttleId is not dynamic (msg type)
          const nodeProject = n.projectType !== 'msg' ? n.projectType : null
          const thisShuttleId = (n.shuttleIdType === 'str') ? n.shuttleId : (n.shuttleIdType === 'project name') ? nodeProject : null
          if (shuttleId === thisShuttleId) {
            if (status) {
              RED.nodes.getNode(n.id).status(status)
            } else {
              RED.nodes.getNode(n.id).status({})
            }
          }
        }
      })
    }
    /*
     * Starting a new runtime:
     *
     * 1. Install node-red version (if not present) to ./node-red/<version or tag>:
     *    npm install --prefix ./node-red/<version or tag> node-red@<version or tag>
     * 2. Create runtime directory
     * 3. Create projects directory within the runtime directory
     * 4. Create symbolic link inside the projects directory linking to the project that should be started
     * 5. Run node ./node-red/<version or tag>/node_modules/node-red/red.js -u ./runtime/<id>/ <project name> using child_process.fork()
     */
    async function start (shuttleId, options) {
      // Directory structure
      const userDir = RED.settings.userDir
      const shuttleDir = path.join(userDir, 'shuttle-red')
      const nodeRedDir = path.join(shuttleDir, 'node-red')
      const nodeRedVersion = node.runtime.version.substring(node.runtime.version.indexOf(':') + 1)
      const versionIsTag = node.runtime.version.startsWith('tag:')
      const nodeRedVersionDir = path.join(nodeRedDir, nodeRedVersion)
      const runtimeDir = path.join(shuttleDir, 'runtime')
      const instanceDir = path.join(runtimeDir, shuttleId)
      const instanceProjectFile = path.join(instanceDir, 'package.json')
      const projectsDir = path.join(userDir, 'projects')
      const instanceProjectsDir = path.join(instanceDir, 'projects')
      const instanceModulesDir = path.join(instanceDir, 'node_modules')
      const instanceShuttleRedDir = path.join(instanceModulesDir, 'shuttle-red')
      const modulesShuttleRedDir = path.join(__dirname, '..')

      const linkTo = path.join(projectsDir, options.projectName)
      const linkFrom = path.join(instanceProjectsDir, options.projectName)
      if (Object.prototype.hasOwnProperty.call(shuttles, shuttleId)) {
        throw new Error('[shuttle-red] Could not start shuttle: An instance with the ID "' + shuttleId + '" is already running.')
      }
      // Check if directory structure has been initialized
      const shuttleDirExists = fs.existsSync(shuttleDir) && fs.existsSync(nodeRedDir) && fs.existsSync(runtimeDir)
      if (!shuttleDirExists) {
        fs.mkdirSync(nodeRedDir, { recursive: true })
        fs.mkdirSync(runtimeDir, { recursive: true })
      }
      // Check if node-red version is already installed
      const isAlreadyInstalled = fs.existsSync(nodeRedVersionDir)
      if (!isAlreadyInstalled) {
        node.status({ fill: 'yellow', shape: 'ring', text: 'Install Node-Red version' })
        await npm.install(nodeRedVersion, nodeRedDir)
      } else if (versionIsTag) {
        // Check if the node-red version can be updated (is it a tag?)
        node.status({ fill: 'yellow', shape: 'ring', text: 'Update Node-Red version' })
        await npm.update(nodeRedVersion, nodeRedDir)
      }
      // Check if instance folder has been initialized
      if (!fs.existsSync(instanceDir)) {
        fs.mkdirSync(instanceModulesDir, { recursive: true })
        // Create symbolic link to shuttle-RED module
        fs.symlinkSync(modulesShuttleRedDir, instanceShuttleRedDir)
        // Create projects file
        fs.writeFileSync(instanceProjectFile, `
          {
            "name": "shuttle ${shuttleId}",
            "description": "A Node-RED instance started from within Node-RED. We call it a shuttle.",
            "author": "The shuttle commander :o)",
            "version": "1.0.0",
            "dependencies": {
              "shuttle-red": "*"
            }
          }
        `)
        // Create symbolic link to project
        if (!fs.existsSync(linkTo)) {
          node.error('[shuttle-red] Could not start runtime: Project "' + options.projectName + '" does not exist.')
          node.status(errorStatus)
          return
        }
        fs.mkdirSync(instanceProjectsDir, { recursive: true })
        fs.symlinkSync(linkTo, linkFrom)
      }
      // Determine environment variables
      const env = { NR_RUNTIME: nodeRedVersionDir, SHUTTLE_ID: shuttleId }
      Object.assign(env, process.env);
      [...node.runtime.environment, ...node.environment].forEach((envVariable) => {
        if (Object.prototype.hasOwnProperty.call(envVariable, 'type')) {
          env[envVariable.key] = RED.util.evaluateNodeProperty(envVariable.value, envVariable.type, node, options.msg)
        }
      })
      // Run node
      const runtime = options.msg.runtime || path.join(nodeRedVersionDir, 'node_modules', 'node-red', 'red.js')
      const runtimeOptions = options.msg.runtime_options || ['-u', instanceDir, '-p', options.port, '-D', 'editorTheme.projects.enabled=true', options.projectName]
      const shuttleProcess = fork(
        runtime,
        runtimeOptions,
        {
          silent: true,
          env
        }
      )
      shuttleProcess.stdout?.on('data', (data) => {
        console.log('[shuttle-red] Shuttle ' + shuttleId, data.toString())
      })
      shuttleProcess.stderr?.on('data', (data) => {
        console.error('[shuttle-red] [Shuttle ' + shuttleId + ']', data.toString())
      })
      shuttleProcess.on('error', (error) => {
        console.error(error)
      })
      // Route message
      shuttleProcess.on('message', (message) => {
        listener.forEach((receive) => {
          receive(message, shuttleId)
        })
      })
      return shuttleProcess
    }

    async function stop (shuttleId, _msg) {
      if (!Object.prototype.hasOwnProperty.call(shuttles, shuttleId)) {
        throw new Error('[shuttle-red] Could not stop shuttle. No instance with the id "' + shuttleId + '" is running.')
      }
      return shuttles[shuttleId]?.kill()
    }

    node.on('input', function (msg, send, done) {
      // get node options
      const projectName = (config.projectType === 'msg') ? RED.util.evaluateNodeProperty(config.project, config.projectType, node, msg) : config.projectType
      let shuttleId = (config.shuttleIdType === 'msg') ? RED.util.evaluateNodeProperty(config.shuttleId, config.shuttleIdType, node, msg) : config.shuttleIdType
      if (shuttleId === 'project name') {
        shuttleId = projectName
      }

      node.action = (config.actionType === 'msg') ? RED.util.evaluateNodeProperty(config.action, config.actionType, node, msg) : config.actionType
      if (node.action !== 'start' && node.action !== 'stop' && node.action !== 'restart') {
        throw new Error('[shuttle-red] Invalid action "' + node.action + '".')
      }
      node.runtime = RED.nodes.getNode(config.runtime)
      node.environment = config.environment

      // refresh shuttle context data
      shuttles = getFlowShuttles()

      switch (node.action) {
        case 'start': {
          node.status({ fill: 'green', shape: 'ring', text: 'Starting...' })
          const port = (config.portType === 'msg' || config.portType === 'num') ? RED.util.evaluateNodeProperty(config.port, config.portType, node, msg) : 0 // dynamic
          start(shuttleId, { projectName, port, msg }).then((shuttleProcess) => {
            if (!shuttleProcess) {
              msg.payload = { shuttleId, started: false }
              node.status(errorStatus)
              send(msg)
              done()
            } else {
              shuttles[shuttleId] = shuttleProcess
              msg.payload = { shuttleId, started: shuttleProcess.connected }
              setStatusToAllStartShuttleId(shuttleId, startedStatus)
              send(msg)
              done()
            }
          }).catch((error) => {
            node.warn(error)
            if (shuttles[shuttleId].connected) {
              node.status({ fill: 'yellow', shape: 'ring', text: 'Shuttle already started.' })
            } else {
              node.status(errorStatus)
            }
            msg.payload = { shuttleId, started: false }
            send(msg)
            done()
          })
          break
        }
        case 'restart': {
          if (!shuttles[shuttleId]) {
            node.error('[shuttle-red] Error: Could not restart shuttle. No instance "' + shuttleId + '" is running.')
            node.status(errorStatus)
            msg.payload = { shuttleId, started: false }
            send(msg)
            done()
          } else {
            const port = shuttles[shuttleId].spawnargs[5]
            const projectName = shuttles[shuttleId].spawnargs[8]
            stop(shuttleId, msg).then(() => {
              // Instance could be stopped
              delete shuttles[shuttleId]
              node.status({}) // Restart info for this node would be unnecessary
              setStatusToAllStartShuttleId(shuttleId, startedStatus)
              start(shuttleId, { projectName, port, msg }).then((shuttleProcess) => {
                if (!shuttleProcess) {
                  msg.payload = { shuttleId, started: false }
                  send(msg)
                  done()
                } else {
                  shuttles[shuttleId] = shuttleProcess
                  msg.payload = { shuttleId, started: shuttleProcess.connected }
                  send(msg)
                  done()
                }
              })
            }).catch((error) => {
              node.error(error)
              msg.payload = { shuttleId, started: false }
              send(msg)
              done()
            })
          }
          break
        }
        case 'stop': {
          stop(shuttleId, msg).then((terminated) => {
            if (terminated) {
              delete shuttles[shuttleId]
            }
            node.status({})
            setStatusToAllStartShuttleId(shuttleId, {})
            msg.payload = { shuttleId, stopped: terminated }
            send(msg)
            done()
          }).catch((error) => {
            node.error(error)
            node.status({})
            msg.payload = { shuttleId, stopped: false }
            if (!shuttles[shuttleId]?.connected) {
              // connection was killed on another way (timeout, critical error,...)
              delete shuttles[shuttleId]
            }
            send(msg)
            done()
          })
          break
        }
        default: {
          node.error('[shuttle-red] [shuttle-red] Error: Unknown action "' + node.action + "'.")
        }
      }
    })
  }

  RED.nodes.registerType('shuttle-control', ShuttleControlNode)

  let projects = []
  const userDir = RED.settings.userDir
  const projectsDir = path.join(userDir, 'projects')
  function getProjects () {
    projects = []
    if (!fs.existsSync(projectsDir)) {
      fs.mkdirSync(projectsDir, { recursive: true })
    }
    fs.readdirSync(projectsDir, { withFileTypes: true }).forEach((fileInfo) => {
      if (
        fileInfo.isDirectory() ||
              (
                fileInfo.isSymbolicLink() &&
                fs.statSync(fs.readlinkSync(path.join(projectsDir, fileInfo.name))).isDirectory()
              )
      ) {
        projects.push(fileInfo.name)
      }
    })
  }
  RED.httpAdmin.get('/shuttle-red/reloadProjects', function (_request, response) {
    getProjects()
    response.send(projects)
  })
  RED.httpAdmin.get('/shuttle-red/getProjects', function (_request, response) {
    if (projects.length === 0) {
      getProjects()
    }
    response.send(projects)
  })
}
